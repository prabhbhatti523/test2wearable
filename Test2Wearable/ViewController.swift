//
//  ViewController.swift
//  Test2Wearable
//
//  Created by Prabhjinder Singh on 2019-11-07.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    
    let USERNAME = "prabhjinderbhatti@gmail.com"
    let PASSWORD = "12345"
    
    let DEVICE_ID = "2e003e001047363333343437"
    var myPhoton : ParticleDevice?
    
    

    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var timeSlowsDownLabel: UILabel!
    
    @IBOutlet weak var timeSlider: UISlider!
    var count = 0
    var counterTimer = Timer()
    
    var sliderValue = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    
    
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                
            }
            
        } // end getDevice()
    }
    func showFace(time: Int) {
        let parameters = ["\(time)"]
        var task = myPhoton!.callFunction("showFace", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
        }
        
    }
    

    @IBAction func startButtonPressed(_ sender: Any) {
      //  showFace()
        self.count = 0
        print("slider value: \(self.sliderValue)")
        self.showFace(time: self.sliderValue)
        startCounter()
    }
    func startCounter(){
        
      
        counterTimer = Timer.scheduledTimer(timeInterval: TimeInterval(self.sliderValue), target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
        
        
    }
    @objc func incrementCounter(){
        
        
        if(self.count < 20){
           
        self.count = self.count + 1
        }
        self.timeLabel.text = "\(self.count)"
    }
    
    @IBAction func sliderActionOccured(_ sender: UISlider) {
        self.sliderValue = Int(timeSlider.value)
        
        self.timeSlowsDownLabel.text = "Time slows down by: \(self.sliderValue)"
    }
    
}

